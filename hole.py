# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import MDAnalysis as mda
import os
import os.path
import subprocess
import seaborn as sns


import warnings
warnings.filterwarnings('ignore')


radius_def = '''
remark: van der Waals radii from XPLOR parameter file:param19x.pro
remark: VDWR determined as sigma/2
remark: Kindly provided by Ian Kerr, Molecular Biophysics, Oxford University
remark:
remark: CB ALA = CH3E, CB THR, VAL, ILE = CH1E
remark: CB ??? = CH2E
VDWR CB   ALA 1.929
VDWR CB   THR 2.107
VDWR CB   VAL 2.107
VDWR CB   ILE 2.107
VDWR CB   ??? 1.991
remark: other CH2E atoms (carbon with two aliphatic hydrogens)
VDWR CA   GLY 1.991
VDWR CG   GLU 1.991
VDWR CG   LYS 1.991
VDWR CD   LYS 1.991
VDWR CE   LYS 1.991
VDWR CG   PRO 1.991
VDWR CD   PRO 1.991
VDWR CG   MET 1.991
VDWR CG1  ILE 1.991
VDWR CG   GLN 1.991
VDWR CG   ARG 1.991
VDWR CD   ARG 1.991
remark: CH3E atoms (carbon with three aliphatic hydrogens)
remark: smaller than CH2E ? I'm only copying from Xplor
VDWR CD?  LEU 1.929
VDWR CE   MET 1.929
VDWR CG2  THR 1.929
VDWR CD1  ILE 1.929
VDWR CG2  ILE 1.929
VDWR CG?  VAL 1.929
remark: aromatic carbon atoms
VDWR CE3  TRP 1.871
VDWR CD1  TRP 1.871
VDWR CZ?  TRP 1.871
VDWR CH2  TRP 1.871
VDWR CD?  PHE 1.871
VDWR CE?  PHE 1.871
VDWR CD?  TYR 1.871
VDWR CE?  TYR 1.871
VDWR CD2  HIS 1.871
VDWR CE1  HIS 1.871
VDWR CZ   ??? 1.871
remark: ASN, GLN polar H (odd names for these atoms in xplor)
VDWR E2?  GLN 0.713
VDWR D2?  ASN 0.713
remark: lysine terminal hydrogens
VDWR HZ?  LYS 0.535
remark: general last, all oxygens have same vdwr in XPLOR
remark: all sulphurs have same vdwr in XPLOR
remark: all nitrogens same vdwr IN XPLOR
remark: all other H will be polar - same vdwr
remark: remaining C, eg. CA, carbonyl C
VDWR C??? ??? 1.871
VDWR H??? ??? 0.713
VDWR O??? ??? 1.425
VDWR S??? ??? 1.684
VDWR N??? ??? 1.425
VDWR P??? ??? 1.800
'''


def clean_pdb(archivo):
    # correct PDB files and keep only atoms, if there is nothing, no problem
    # I would say to control if I want this or not, in a moment I found it necessary but I do not know
    # Be careful because overwrite the file
    with open(archivo, "r+") as f:
        new_f = f.readlines()
        f.seek(0)
        for line in new_f:
            if 'ATOM' in line:
                f.write(line)
        f.truncate()


# INPUT generator for hole

def gen_input(archivo, center_selection='protein'):
# Call Mddallysis to load the protein, here is
    # Configured to read Mútiples PDBS
    u = mda.Universe(archivo)

    # recupero el nombre de archivo sin la extensión
    basename = os.path.basename(archivo).split('.')[0]

    # the input will be the combination of the PDB file plus the suffix _hole.inp
    hole_input_name = basename + '.inp'

    # the selection that defines the center, MDAnalysis should be consulted for
    center_selection = "resnum 57 193"
# understand the selection language

    center_residues = u.select_atoms(center_selection)
    # type of center, in this case mass
    center_coord = center_residues.center_of_mass()

    # las coordenadas x y z
    center_x = float(np.split(center_coord, 3)[0])
    center_y = float(np.split(center_coord, 3)[1])
    center_z = float(np.split(center_coord, 3)[2])

    # radios file, which is hardcoded here at the beginning

    radius = '/home/juan/bin/hole2/rad/xplor.rad'  # el archivo de radios

    with open("xplor.rad", "w") as radius_file:
        radius_file.write(radius_def)

    # one much more complete is ~/hole2/rad/xplor.rad

    sphpdb = 'spherical.sph'  # standard hole spherical probe in this run
    ignore = 'SOL WAT TIP HOH K NA CL'  # residues to ignore, only supports three letters
    cvect = '0 0 1'  # channel runs in Z direction
    endrad = 5.0  # ultimo valor de radio a tomar en cuenta
    sample = 0.01  # distance between planes
    # point density. Dotden should be set to between 5 (few dots per sphere) and 35 (large number of dots per sphere). The default value is 10.
    dotden = 30

    # create input file for Hole
    hole_input_file = open(hole_input_name, "w")

    input_content = ["COORD ", archivo, "\n",
                     "CPOINT ", str(center_x), " ", str(
                         center_y), " ", str(center_z), " ", "\n",
                     "CVECT ", cvect, "\n",
                     "SAMPLE ", str(sample), "\n",
                     "RADIUS ", radius, "\n",
                     "SPHPDB ", sphpdb,  "\n",
                     "IGNORE ", ignore, "\n",
                     "ENDRAD ", str(endrad),  "\n",
                     "DOTDEN ", str(dotden),  "\n"]

    # I write the previous lines in the input
    hole_input_file.writelines(input_content)

    hole_input_file.close()  # I close the file, typical of Python


# Pore profile generator

def get_profile(outfile='hole.out', minrad=0.2):

    # I read the file and get the start
    basename = os.path.basename(outfile)

   # will be the output file in CSV format
    clean_output = basename + '_profile.csv'
    min_radius_out = basename + '_minradio.txt'

    data = None # I create the data with None or nothing

    with open(outfile, "r") as f:  # cargo el archivo
        for line in f:  # me muevo por cada línea
            if data is None:  # veo si no hay nada
                if line.startswith(" cenxyz.cvec"):  # Busco el comienzo de la tabla
                    data = []  # hago data vacío
            # la tabla se termina y no hay nada más
            elif line.startswith(" Minimum"):
                break  # así que paro acá
            else:  # si estoy leyendo la tabla, meter cada valor en la variable data
                data.append(line)  # con un append

        array = np.array(data)  # lo transformo en un array de numpy

        df_series = pd.Series(array).str.split(
            expand=True)  # ahora es una Serie de Pandas
       # this Series is saved in a CSV file to retrieve or do things
       # but I found out that sometimes he throws things that don't make sense,
       # that is, there cannot be negative radii so I put a control variable to eliminate those things
       # so I put a conditional that if the minimum is negative don't save it

        min_radius = df_series.min().to_numpy()

        if min_radius > minrad:

            df_series.to_csv(clean_output, 
                            columns=[0, 1, 2], 
                            header=[ 'Vector XYZ', 'Radius', 'Coordinate'], 
                            index=False)


def load_data():
    filepaths = [f for f in os.listdir(".") if f.endswith('.csv')]

    full_df = pd.concat(map(pd.read_csv, filepaths))
    full_df.dropna(inplace=True)
    full_df.sort_values(by=['Coordinate'], inplace=True)

    full_df.to_csv('Full_profile_pore.csv', 
                    header=['Vector XYZ', 'Radius', 'Coordinate'], 
                    index=False)


# Bash script generator

def gen_bash():
    bash_script = '''\
#!/bin/bash

for input in *.inp; do

/home/juan/bin/hole2/exe/hole < $input > ${input}_.out

done

'''
    with open('hole.sh', 'w') as rsh:
        rsh.write(bash_script)
    os.chmod('hole.sh', 0o755)


# Correr script de bash

def run_script_bash(script_path='./hole.sh'):
    subprocess.call(script_path, shell=True)


# Graficar

def histogram(csv_file='Full_profile_pore.csv', bins=1000, average=False, output='Chain.png'):

    df = pd.read_csv(csv_file)

    x = df['Coordinate']
    y = df['Radius']
    y_min = y.min()

    if average:
        df['Radius Average'] = df.Radius.rolling(bins).mean()
        z = df['Radius Average']
        z_min = z.min()

    plot = sns.lineplot(x=x, y=z,
                        alpha=1,
                        label='Pore Profile',
                        ci=80)

    fig = plot.get_figure()
    fig.savefig('hist.png')

    x_binned = pd.cut(x, bins)
    y_binned = pd.cut(y, bins)


# with this it should start to process the files
# Read and clean the PDB files, then generate the inputs
print("1. Empiezo a procesar los archivos PDB")
for archivo in os.listdir():
    if archivo.endswith('.pdb'):
     # correct the PDB files and keep only the ATOMs, if there is nothing, no problem
    # I would say to control if I want this or not, at one point it seemed necessary but I no longer know
        clean_pdb(archivo)
        gen_input(archivo)
print("Terminé de generar los archivos INP")
print("2. Ahora genero el script de bash...")
gen_bash()

print("Y lo corro...")
run_script_bash('./hole.sh')
print("3. Ya terminó, entonces me pongo a analizar los datos de salida y recopilar los perfiles de poro")

for calculo in os.listdir():
    if calculo.endswith('_.out'):
        get_profile(outfile=calculo)

load_data()

print('4. Genero los gráficos')

histogram(csv_file='Full_profile_pore.csv', bins=1000, output='ChainA.png')

print("Parece que ya está :)")
